from django.shortcuts import render, get_object_or_404, redirect
from receipts.models import ExpenseCategory, Account, Receipt
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, CategoryForm, AccountForm
# Create your views here.

@login_required
def receipt_list(request):
    all_receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "all_receipts": all_receipts
    }
    return render(request, "list_receipts.html", context)


@login_required
def account_list(request):
    my_accounts = Account.objects.filter(owner=request.user)

    context = {
        "all_accounts": my_accounts
    }
    return render(request, "account_list.html", context)


@login_required
def category_list(request):
    all_receipts = Receipt.objects.filter(purchaser=request.user)
    count_each = {}
    for receipt in all_receipts:
        if receipt.category.name not in count_each:
            count_each[receipt.category.name] = 0
        count_each[receipt.category.name] += 1

    context = {
        "counting_dict": count_each
    }
    return render(request, "category_list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt_item = form.save(False)
            receipt_item.purchaser = request.user
            receipt_item.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form
        }
    return render(request, "create_receipt.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            step_form = form.save(False)
            step_form.owner = request.user
            step_form.save()
            return redirect("category_list")
    else:
        form = CategoryForm()
    context = {
        "form": form
    }
    return render(request, "create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            step_form = form.save(False)
            step_form.owner = request.user
            step_form.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form
    }
    return render(request, "create_account.html", context)
