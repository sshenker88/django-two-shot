from django.urls import path
from .views import receipt_list, create_receipt, category_list, account_list, create_category, create_account


urlpatterns = [
    path("", receipt_list, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", category_list, name="category_list"),
    path("accounts/", account_list, name="account_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
    # path("<int:id>/", show_recipe, name="show_recipe"),
    # path("create", create_recipe, name="create_recipe"),
    # path("<int:id>/edit/", edit_recipe, name = "edit_recipe"),
    # path("mine/", my_recipe_list, name="my_recipe_list")

]
